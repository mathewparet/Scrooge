<?php

use App\Models\Author;
use App\Models\Book;
use App\Models\Series;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private $tables = [
        'users' => User::class,
        'books' => Book::class,
        'series' => Series::class,
        'authors' => Author::class,
        'tags' => Tag::class,
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->addSlugColumnToTables();

        $this->migrateExistingData();

        $this->makeSlugsUnique();
    }

    private function migrateExistingData()
    {
        foreach ($this->tables as $table => $class) {
            $class::get()->each(function ($item) {
                $item->save();
            });
        }
    }

    private function addSlugColumnToTables()
    {
        foreach (array_keys($this->tables) as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->string('slug');
            });
        }
    }

    private function makeSlugsUnique()
    {
        foreach (array_keys($this->tables) as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->string('slug')->unique()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (array_keys($this->tables) as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('slug');
            });
        }
    }
};
