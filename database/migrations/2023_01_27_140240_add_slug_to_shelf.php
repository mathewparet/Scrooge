<?php

use App\Models\Shelf;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createSlugField();

        $this->generateSlugsForExistingShelves();

        $this->makeSlugFieldUnique();
    }

    private function generateSlugsForExistingShelves()
    {
        Shelf::each(function ($shelf) {
            $shelf->save();
        });
    }

    private function createSlugField()
    {
        Schema::table('shelves', function (Blueprint $table) {
            $table->string('slug')->nullable();
        });
    }

    private function makeSlugFieldUnique()
    {
        Schema::table('shelves', function (Blueprint $table) {
            $table->string('slug')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shelves', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
};
