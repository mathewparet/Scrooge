<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->date('published_at');
            $table->text('description');
            $table->json('identifiers')->nullable();
            $table->string('path')->index();
            $table->integer('rating')->default(0);
            $table->integer('page_count')->nullable();
            $table->string('image');
            $table->integer('series_position')->nullable();
            $table->integer('external_id')->index();
            $table->foreignId('series_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
};
