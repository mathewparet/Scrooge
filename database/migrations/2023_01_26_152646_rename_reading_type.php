<?php

use App\Models\Shelf;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Shelf::where('type', 'CURRENTLY_READING')->update([
            'type' => 'READING',
            'name' => 'Reading',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Shelf::where('type', 'READING')->update([
            'type' => 'CURRENTLY_READING',
            'name' => 'Currently Reading',
        ]);
    }
};
