<?php

use App\Models\Shelf;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->defineShelvesTable();

        $this->createInitialShelvesForExistingUsers();
    }

    private function createInitialShelvesForExistingUsers()
    {
        User::each(function ($user) {
            Shelf::createForUser($user);
        });
    }

    private function defineShelvesTable()
    {
        Schema::create('shelves', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('access')->index()->default('private');
            $table->foreignId('user_id');
            $table->string('type')->index();
            $table->timestamps();
        });

        Schema::create('book_shelf', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('shelf_id');
            $table->foreignId('book_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_shelf');

        Schema::dropIfExists('shelves');
    }
};
