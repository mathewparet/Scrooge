<?php

use App\Http\Controllers\Admin\InvitationController as AdminInvitationController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\ConversionController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\DeviceController;
use App\Http\Controllers\MarkdownDocumentController;
use App\Http\Controllers\PublicShelfController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SeriesController;
use App\Http\Controllers\ShelfController;
use App\Http\Controllers\TagController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Home page routes
 */
Route::redirect('/', '/books');

/**
 * Legal routes - License, Privacy Policy, and Terms of service
 */
Route::get('/license', [MarkdownDocumentController::class, 'license'])->name('license');
Route::get('/terms-of-service', [MarkdownDocumentController::class, 'terms'])->name('terms.show');
Route::get('/privacy-policy', [MarkdownDocumentController::class, 'privacy'])->name('privacy.show');

/**
 * Below routes are accessible only by authenticated and verified users
 */
Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    /**
     * Books
     */
    Route::get('/covers/{path}', [BookController::class, 'cover'])->name('books.cover')->where('path', ".*\.jpg");
    Route::get('/books/{book}/download/{format}', [BookController::class, 'download'])->name('books.download');
    Route::post('/books/{book}/send/{device}/{format}', [BookController::class, 'send'])->name('books.send');
    Route::resource('/books', BookController::class)->only(['index', 'show']);

    /**
     * Ebook Convertion
     */
    Route::post('/convert', ConversionController::class)->name('convert');

    /**
     * Search
     */
    Route::get('/search', SearchController::class)->name('search');

    /**
     * Authors
     */
    Route::resource('/authors', AuthorController::class)->only(['index', 'show']);

    /**
     * Tags
     */
    Route::resource('/tags', TagController::class)->only(['index', 'show']);

    /**
     * Series
     */
    Route::resource('/series', SeriesController::class)->only(['index', 'show']);

    /**
     * Logged in user related routes
     */
    Route::prefix('user')->group(function () {
        /**
         * Send to device
         */
        Route::post('devices/{device}/deliver', DeliveryController::class)->name('devices.deliver');

        /**
         * Devices
         */
        Route::resource('devices', DeviceController::class)->except(['show']);

        /**
         * Shelves
         */
        Route::post('shelves/{shelf}/toggle', [ShelfController::class, 'toggle'])->name('shelves.toggle');
        Route::resource('shelves', ShelfController::class);
    });

    /**
     * Public shelf listing
     */
    Route::get('/shelves', [PublicShelfController::class, 'index'])->name('public.shelves.index');

    /**
     * Public shelf page under user's public profile
     */
    Route::get('/users/{user}/shelves/{shelf:slug}', [PublicShelfController::class, 'show'])->name('public.shelves.show');

    /**
     * Administration
     */
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('users', AdminUserController::class);
        Route::post('invitations/{invitation}/resend', [AdminInvitationController::class, 'resend'])->name('invitations.resend');
        Route::resource('invitations', AdminInvitationController::class);
    });
});
