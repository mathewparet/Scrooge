# Scrooge - your companion for Calibre

## Licence

The MIT License (MIT) Copyright © Mathew Paret

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## System Requirements

ScroogeRead runs on Laravel 9.x. All [requirements to run a Laravel 9.x application need](https://laravel.com/docs/9.x/deployment#server-requirements) to be met.

> ScroogeRead uses certain `PHP 8.1` features. So you will need to install `PHP 8.1` and it's modules instead of the `PHP8.0` and it's modules as per Laravel Requirements.

Apart from that, the below requirements also need to be met:

1. An [`Algolia` ID & KEY](https://algolia.com) or install [`Meilisearch`](https://www.meilisearch.com/).
1. `Memcached` or `Redis`
1. `php8.1-sqlite3`, `php8.1-zip`, `php8.1-intl`, `php8.1-curl`
1. Install and configure [Calibre](https://calibre-ebook.com/download).

## Initial Setup

1. Clone this repo - `git clone https://mathewparet@bitbucket.org/mathewparet/scrooge.git`.
1. Run `composer install --no-dev` within the deployment directory.
1. Make a copy of `.env.example` as `.env`.
1. Generate an application key `php artisan key:generate`.
    
    > Remember to note down this key, if you lose it, you will not be able to recover encrypted or hashed data.

1. Link the required directories to public - `php artisan storage:link`.
1. Edit the `.env` file. Comments are provided in the file to guide you.
1. Create database tables - `php artisan migrate`.
1. Get the invitation link to create the super user account - `php artisan create-admin <your-email-address>`.
1. Generate the required caches:
    ```
    php artisan optimize
    php artisan view:cache
    php artisan event:cache
    ```
1. Schedule a cronjob to run every minute. The command to be called is `php /path/to/deployment/artisan schedule:run`.
1. Schedule the job queue using [Supervisor](https://laravel.com/docs/9.x/queues#supervisor-configuration) or [Horizon](https://laravel.com/docs/9.x/horizon).
1. Run your first library import - `php artisan library:import`.

    > Subsequent runs will automatically run every 15 minutes as long as step 10 above is configired.

## Upgrades

1. Put site in maintenance mode - `php artisan down --retry=5 --refresh=5 --render=errors.503`.
    
    > Read more about maintenance mode [here](https://laravel.com/docs/9.x/configuration#maintenance-mode).

1. Clear the application cache - `php artisan cache:clear`
1. Pull the latest changes - `git pull`.
1. Install new dependencies - `composer install --no-dev`.
1. Run new migrations - `php artisan migrate`.
1. Generate the required caches:
    ```
    php artisan optimize
    php artisan view:cache
    php artisan event:cache
    ```
1. Restart the job queue - `php artisan queue:restart` or `php artisan horizon:restart` based on the queue manager you've configured.
1. Bring application out of maintenance mode - `php artisan up`.