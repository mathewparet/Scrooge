<?php

use App\Device\Email;
use App\Device\GoogleDrive;

return [

    'kindle' => [
        'driver' => Email::class,
        'config' => [
            'limits' => [
                'formats' => array_map('trim', explode(',', env('KINDLE_FILE_FORMATS', 'epub,pdf'))),
                'size' => env('KINDLE_MAX_FILE_SIZE', 50), // in Megabytes
            ],
            'help' => 'https://www.amazon.com/gp/help/customer/display.html?nodeId=G7V489F2ZZU9JJGE',
        ],
    ],

    'email' => [
        'driver' => Email::class,
        'config' => [

        ],
    ],

    // 'google_drive_folder' => [
    //     'driver' => GoogleDrive::class,
    //     'config' => [
    //         'callback' => 'http://localhost/drive-api/callback.php',
    //         'keys' => [
    //             'id' => env("GOOGLE_CLIENT_ID"),
    //             'secret' => env("GOOGLE_CLIENT_SECRET"),
    //         ],
    //         'scope' => 'https://www.googleapis.com/auth/drive',
    //         'authorize_url_parameters' => [
    //             'approval_prompt' => 'force', // to pass only when you need to acquire a new refresh token.
    //             'access_type' => 'offline'
    //         ],
    //     ]
    // ],
    //
    // 'kobo' => [
    //     'driver' => GoogleDrive::class,
    //     'config' => [
    //         'callback' => 'http://localhost/drive-api/callback.php',
    //         'keys' => [
    //             'id' => env("GOOGLE_CLIENT_ID"),
    //             'secret' => env("GOOGLE_CLIENT_SECRET"),
    //         ],
    //         'scope' => 'https://www.googleapis.com/auth/drive',
    //         'authorize_url_parameters' => [
    //             'approval_prompt' => 'force', // to pass only when you need to acquire a new refresh token.
    //             'access_type' => 'offline'
    //         ],
    //     ]
    // ],
    //
    // 'onyx_boox' => [
    //     'driver' => GoogleDrive::class,
    //     'config' => [
    //         'callback' => 'http://localhost/drive-api/callback.php',
    //         'keys' => [
    //             'id' => env("GOOGLE_CLIENT_ID"),
    //             'secret' => env("GOOGLE_CLIENT_SECRET"),
    //         ],
    //         'scope' => 'https://www.googleapis.com/auth/drive',
    //         'authorize_url_parameters' => [
    //             'approval_prompt' => 'force', // to pass only when you need to acquire a new refresh token.
    //             'access_type' => 'offline'
    //         ],
    //     ]
    // ],
];
