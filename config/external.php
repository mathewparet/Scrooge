<?php

use App\ExternalLibrary\Drivers\Calibre\Calibre;

return [

    'default' => env('EXTERNAL_LIBRARY', 'calibre'),

    'providers' => [
        'calibre' => Calibre::class,
    ],

];
