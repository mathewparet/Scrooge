<?php

return [
    'required' => (bool) env('INVITATION_REQUIRED', true),
    'expiry' => env('INVITATION_EXPIRES_IN_HOURS', 7),
];
