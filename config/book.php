<?php

use App\BookAPI\Providers\GoogleBooksAPI\GoogleBooksAPI;

return [

    /**
     * Book API providers and configs
     */
    'default' => env('BOOKS_API_PROVIDER', 'google'),

    'providers' => [
        /**
         * Google Books API provider
         */
        'google' => [
            'driver' => GoogleBooksAPI::class,
            'endpoint' => 'https://www.googleapis.com/books/v1/volumes',
        ],

    ],

    'identifiers' => [
        'asin' => [
            'label' => 'Amazon',
            'url' => 'https://amazon.com/dp/:identifier',
        ],
        'goodreads' => [
            'label' => 'Goodreads',
            'url' => 'http://goodreads.com/book/show/:identifier',
        ],
        'isbn' => [
            'label' => 'ISBN 10',
            'url' => 'https://www.worldcat.org/isbn/:identifier',
        ],
        'isbn13' => [
            'label' => 'ISBN 13',
            'url' => 'https://www.worldcat.org/isbn/:identifier',
        ],
        'google' => [
            'label' => 'Google Books',
            'url' => 'https://books.google.com/books?id=:identifier',
        ],
    ],
];
