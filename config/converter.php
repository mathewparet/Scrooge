<?php

use App\EbookConverter\CalibreConverter;

return [
    'default' => env('EBOOK_CONVERTER', 'calibre'),

    'providers' => [
        'calibre' => [
            'driver' => CalibreConverter::class,
            'binary' => env('CALIBRE_BINARY', '/usr/bin/ebook-convert'),
        ],
    ],

    'manual' => (bool) env('EBOOK_CONVERTION_MANUAL', false),

    'auto' => (bool) env('EBOOK_CONVERTION_AUTO', false),

    'output' => [
        'auto' => explode(',', env('EBOOK_CONVERT_TO_AUTO', 'epub')),
        'all' => explode(',', env('EBOOK_CONVERT_TO_MANUAL', 'epub,mobi,pdf')),
    ],

    'input' => explode(',', env('EBOOK_CONVERT_FROM', 'epub,mobi')),

    'formats' => explode(',', env('EBOOK_SUPPORTED_FORMATS', 'epub,pdf,mobi,txt')),
];
