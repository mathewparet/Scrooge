<?php

namespace App\Mail;

use App\Models\Invitation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class InvitationToRegister extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $invitation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invitation $invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: 'Invitation to create an account on '.config('app.name'),
            to: $this->invitation->email,
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        $user = $this->invitation->user;

        $expiry = config('invitation.expiry');

        $url = $this->getInvitationUrl($expiry);

        return new Content(
            markdown: 'Mail.InvitationToRegister',
            with: compact('user', 'expiry', 'url')
        );
    }

    private function getInvitationUrl($expiry)
    {
        return URL::temporarySignedRoute('register', now()->addDays($expiry), [
            'invitation_id' => $this->invitation->id,
            'hash' => $this->invitation->getHash(),
        ]);
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
