<?php

namespace App\Policies;

use App\Enums\ShelfAccess;
use App\Enums\ShelfType;
use App\Models\Shelf;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShelfPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        return in_array($ability, ['delete']) ? null : ($user->isSuperUser() ?: null);
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Shelf $shelf)
    {
        return $user->owns($shelf)
            || $shelf->access == ShelfAccess::PUBLIC;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Shelf $shelf)
    {
        return $user->owns($shelf);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Shelf $shelf)
    {
        return $user->owns($shelf)
            && $shelf->type == ShelfType::CUSTOM;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Shelf $shelf)
    {
        return $user->owns($shelf);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Shelf $shelf)
    {
        return $user->owns($shelf)
            && $shelf->type == ShelfType::CUSTOM;
    }
}
