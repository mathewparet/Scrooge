<?php

namespace App\EbookConverter\Facades;

use App\EbookConverter\Contracts\EbookConverter as ContractsEbookConverter;
use Illuminate\Support\Facades\Facade;

/**
 * @method static array inputFormats() Get supported input formats
 * @method static array autoOutputFormats() Get automated conversion output formats
 * @method static array allOutputFormats() Get all supported output formats
 * @method static void convert(\App\Models\File $file, string $to) Convert given file to given format
 * @method static array allMissingFormats(array $available_formats) Get all missing file formats
 * @method static array autoMissingFormats(array $available_formats) Get automatic missing file formats
 */
class EbookConverter extends Facade
{
    public static function getFacadeAccessor()
    {
        return ContractsEbookConverter::class;
    }
}
