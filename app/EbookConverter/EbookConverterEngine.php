<?php

namespace App\EbookConverter;

use App\Exceptions\EbookConvertionError;
use App\Models\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

abstract class EbookConverterEngine
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Get automated conversion output formats
     *
     * @return array
     */
    public function autoOutputFormats()
    {
        return config('converter.output.auto');
    }

    /**
     * Get all supported output formats
     *
     * @return array
     */
    public function allOutputFormats()
    {
        return config('converter.output.all');
    }

    /**
     * Get supported input formats
     *
     * @return array
     */
    public function inputFormats()
    {
        return config('converter.input');
    }

    /**
     * Get all missing formats
     *
     * @param  array  $available_formats
     * @return array $missing_formats
     */
    public function allMissingFormats($available_formats)
    {
        return array_values(array_diff($this->allOutputFormats(), $available_formats));
    }

    /**
     * Get automatic missing formats
     *
     * @param  array  $available_formats
     * @return array $missing_formats
     */
    public function autoMissingFormats($available_formats)
    {
        return array_values(array_diff($this->autoOutputFormats(), $available_formats));
    }

    private function getDestinationFilename(File $file, $to)
    {
        $path_parts = pathinfo($file->path);
        $to_file = Str::finish($path_parts['dirname'], '/').$path_parts['filename'].'.'.$to;

        return $to_file;
    }

    public function convert(File $file, $to)
    {
        try {
            $from_path = Storage::disk('library')->path($file->path);
            $to_path = Storage::disk('library')->path($this->getDestinationFilename($file, $to));

            $output = app()->call([$this, 'translate'], [
                'from' => $from_path,
                'to' => $to_path]);

            if (! $output) {
                throw new EbookConvertionError('Error converting ebook - no output was returned by the converter.');
            }
        } catch(\Throwable $e) {
            Log::error(__CLASS__."->convert({$file->path},{$to}):".$e->getMessage());
            Log::debug(__CLASS__."->convert({$file->path},{$to})", compact('output'));

            throw $e;
        }
    }
}
