<?php

namespace App\EbookConverter;

use App\EbookConverter\Contracts\EbookConverter;
use Illuminate\Support\Facades\Log;

class CalibreConverter extends EbookConverterEngine implements EbookConverter
{
    public function translate($from, $to)
    {
        $application = $this->config['binary'];

        $command = __(':application ":from" ":to"', compact('application', 'from', 'to'));

        Log::debug(__CLASS__, compact('command'));

        return shell_exec($command);
    }
}
