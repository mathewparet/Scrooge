<?php

namespace App\EbookConverter\Contracts;

use App\Models\File;

interface EbookConverter
{
    /**
     * Convert given file to given format
     *
     * @param  \App\Models\File  $file
     * @param  string  $to
     *
     * @throws
     */
    public function convert(File $file, $to);
}
