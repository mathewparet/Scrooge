<?php

namespace App\Http\Controllers;

use App\Enums\ShelfAccess;
use App\Enums\ShelfType;
use App\Http\Requests\StoreShelfRequest;
use App\Http\Requests\ToggleBookOnShelfRequest;
use App\Http\Requests\UpdateShelfRequest;
use App\Models\Book;
use App\Models\Shelf;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;

class ShelfController extends Controller
{
    public function __construct()
    {
        return $this->authorizeResource(Shelf::class, 'shelf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shelf = request()->user()->shelves()->wantToread()->first();

        return redirect()->route('shelves.show', ['shelf' => $shelf->id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Shelf/Create', [
            'types' => ShelfType::cases(),
            'accesses' => ShelfAccess::cases(),
            'book' => request()->get('book'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreShelfRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShelfRequest $request)
    {
        $shelf = $request->user()->shelves()->create($request->only('name', 'access', 'type'));

        if ($request->has('book')) {
            $book = Book::find($request->get('book'));

            $shelf->books()->attach($book);

            return redirect()->route('books.show', ['book' => $book->slug])->with('success', "{$book->title} has been added to {$shelf->name}.");
        }

        return redirect()->route('shelves.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function show(Shelf $shelf)
    {
        $shelves = request()->user()->shelves()->withCount('books')->with('books')->get();

        $current_shelf = $shelf;

        $books = $shelf->books()->with('authors')->orderByPivot('id', 'desc')->paginate(config('app.per_page'));

        return Inertia::render('Shelf/Index', compact('shelves', 'current_shelf', 'books'));
    }

    public function toggle(Shelf $shelf, ToggleBookOnShelfRequest $request)
    {
        $book = Book::find($request->post('book'));

        $shelf->books()->toggle($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function edit(Shelf $shelf)
    {
        $shelf->loadCount('books');

        return Inertia::render('Shelf/Create', [
            'mode' => 'edit',
            'shelf' => $shelf,
            'types' => ShelfType::cases(),
            'accesses' => ShelfAccess::cases(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateShelfRequest  $request
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShelfRequest $request, Shelf $shelf)
    {
        $shelf->update($request->only('name', 'access', 'type'));

        Cache::tags(['public_shelf_list'])->flush();

        return redirect()->route('shelves.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shelf $shelf)
    {
        $shelf->delete();

        return redirect()->route('shelves.index');
    }
}
