<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Book;
use App\SearchProvider\Contracts\SearchProvider;
use Inertia\Inertia;

class SearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(SearchRequest $request, SearchProvider $search_engine)
    {
        try {
            [$query, $filters] = $this->parseQuery($request->get('query'));

            $books = Book::search($query, function ($index, $query, $options) use ($filters, $search_engine) {
                return $search_engine->filter($query, $options, $index, $filters);
            })->query(fn ($inline_query) => $inline_query->with('authors'))
                ->orderBy('created_at', 'desc')
                ->orderBy('published_at', 'desc')
                ->paginate(config('app.per_page'))
                ->withQueryString();
        } catch(\Throwable $e) {
            // Query failed, let's run a search with everything as a query than a filter
            $books = Book::search($request->get('query'))
                ->query(fn ($query) => $query->with('authors'))
                ->orderBy('created_at', 'desc')
                ->orderBy('published_at', 'desc')
                ->paginate(config('app.per_page'))
                ->withQueryString();
        }

        return Inertia::render('Book/Index', ['books' => $books, 'query' => $request->get('query')]);
    }

    /**
     * Get any filters from the input string
     *
     * @param  string  $query
     * @return array
     */
    private function getQueryFilters($query)
    {
        preg_match('/>|=|!=|<|<=|>=|:|IN|NOT|TO|EXISTS/', $query, $matches);

        return $matches;
    }

    /**
     * Check if there is there a plain query before filters start
     *
     * @param  string  $input
     * @param array filters
     * @return bool
     */
    private function aPlainQueryExistsBeforeFilters($input, $filters)
    {
        return preg_match('/AND|OR/', substr($input, 0, strpos($input, $filters[0])), $matches_query);
    }

    /**
     * Parse user input and take retrieve filters and nartural query
     *
     * @param  string  $input
     * @return array [string $query, string $filters]
     */
    private function parseQuery($input)
    {
        $filters = $this->getQueryFilters($input);

        if (! $filters) {
            // everything is a plain query
            return [$input, ''];
        }

        if ($this->aPlainQueryExistsBeforeFilters($input, $filters)) {
            return $this->seperatePlainQueryFromFilters($input);
        } else {
            // everything is a filter
            return ['', $this->formatFilters($input)];
        }

        return ['', $this->formatFilters($input)];
    }

    /**
     * Format filter to support use of colon (:) instead of equals (=)
     *
     * @param  string  $input
     * @return string
     */
    private function formatFilters($input)
    {
        // return str_replace(':', '=', $input);
        return $input;
    }

    /**
     * Seperate plain query from input
     *
     * @param  string  $input
     * @return array [string $query, string $filters]
     */
    private function seperatePlainQueryFromFilters($input)
    {
        preg_match('/AND|OR/', $input, $splits);

        return explode($splits[0], $input, 2);
    }
}
