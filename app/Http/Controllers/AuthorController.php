<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;

class AuthorController extends Controller
{
    public function __construct()
    {
        return $this->authorizeResource(Author::class, 'author');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $current_page = $request->get('page', 1);

        $authors = Cache::tags(["public_author_list_", "book_list"])->remember('public_author_list_'.$current_page, config('cache.bulk'), function () {
            return Author::withCount('books')->orderBy('name', 'asc')->paginate();
        });

        return Inertia::render('Author/Index', compact('authors'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        $books = $author->books()->with('authors')->orderBy('created_at', 'desc')->orderBy('published_at', 'desc')->paginate(config('app.per_page'));
        $type = 'Author: '.$author->name;

        return Inertia::render('Book/Index', compact('books', 'type'));
    }
}
