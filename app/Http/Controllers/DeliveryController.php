<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookDeliveryRequest;
use App\Jobs\SendToDevice;
use App\Models\Device;
use App\Models\File;
use Illuminate\Support\Facades\Log;

class DeliveryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Device $device, BookDeliveryRequest $request)
    {
        $file = File::find($request->file);

        try {
            SendToDevice::dispatch($file, $device);

            Log::debug('Job scheduled to send book to device', [
                'title' => $file->book->title,
                'device' => $device->name,
            ]);
        } catch(\Throwable $e) {
            Log::error('Error sending book to device', [
                'title' => $file->book->title,
                'device' => $device->name,
            ]);

            return redirect()->back()->with('error', __('An error occured while trying to send :title to :device', [
                'title' => $file->book->title,
                'device' => $device->name,
            ]));
        }
    }
}
