<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;

class TagController extends Controller
{
    public function __construct()
    {
        return $this->authorizeResource(Tag::class, 'tag');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $current_page = $request->get('page', 1);

        $tags = Cache::tags(["public_tags_list", "book_list"])->remember('public_tags_list_'.$current_page, config('cache.bulk'), function () {
            return Tag::withCount('books')->orderBy('name')->paginate();
        });

        return Inertia::render('Tag/Index', compact('tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        $books = $tag->books()->with('authors')->orderBy('created_at', 'desc')->orderBy('published_at', 'desc')->paginate(config('app.per_page'));
        $type = 'Tag: '.$tag->name;

        return Inertia::render('Book/Index', compact('books', 'type'));
    }
}
