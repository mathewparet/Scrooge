<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookConversionRequest;
use App\Jobs\AddNewlyConvertedBookToLibrary;
use App\Jobs\ConvertEbookFormat;
use App\Jobs\NotifyUserWhenEbookIsConverted;
use App\Jobs\SendToDevice;
use App\Models\Book;
use App\Models\Device;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;

class ConversionController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(BookConversionRequest $request)
    {
        try {
            $book = Book::find($request->book);

            $source_file = $book->getConvertableFile();

            $device = Device::find($request->device);

            Log::info('User requested file conversion', ['source' => $source_file, 'format' => $request->format]);

            $this->dispatchJobChain($source_file, $request->format, $book, $request->user(), $device);

            if ($device) {
                return redirect()->back()->with('info', __("We are now converting ':book' to :format format. We will send it to :device when it is ready.", [
                    'book' => $book->title,
                    'format' => $request->format,
                    'device' => $device->name,
                ]));
            } else {
                return redirect()->back()->with('info', __("We are now converting ':book' to :format format. We will notify you when it is ready to download.", [
                    'book' => $book->title,
                    'format' => $request->format,
                ]));
            }
        } catch(\Throwable $e) {
            Log::error('An error occured while trying to convert the book', compact('e'));

            return redirect()->back()->with('error', 'An error occured while trying to convert the book. Please try again in some time.');
        }
    }

    private function dispatchJobChain($source_file, $format, $book, $user, $device = null)
    {
        Bus::chain([
            new ConvertEbookFormat($source_file, $format),
            new AddNewlyConvertedBookToLibrary($source_file, $format),
            $device
                ? new SendToDevice($book, $device, $format)
                : new NotifyUserWhenEbookIsConverted($user, $book, $format),
        ])->dispatch();
    }
}
