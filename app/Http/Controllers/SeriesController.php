<?php

namespace App\Http\Controllers;

use App\Models\Series;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $current_page = $request->get('page', 1);

        $series = Cache::tags(["public_series_list", "book_list"])->remember('public_series_list_'.$current_page, config('cache.bulk'), function () {
            return Series::with('latestBook')->withCount('books')->orderBy('name')->paginate();
        });

        return Inertia::render('Series/Index', compact('series'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Series  $series
     * @return \Illuminate\Http\Response
     */
    public function show(Series $series)
    {
        $books = $series->books()->with('authors')->orderBy('series_position', 'desc')->paginate(config('app.per_page'));
        $type = 'Series: '.$series->name;

        return Inertia::render('Book/Index', compact('books', 'type'));
    }
}
