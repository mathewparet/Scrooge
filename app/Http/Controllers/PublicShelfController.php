<?php

namespace App\Http\Controllers;

use App\Models\Shelf;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;

class PublicShelfController extends Controller
{
    public function __construct()
    {
        return $this->authorizeResource(Shelf::class, 'shelf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $shelves = Cache::tags(['public_shelf_list'])->remember('public_shelf_list_'.$request->get('page', 1), config('cache.bulk'), function () {
            return Shelf::public()
                        ->with('user')
                        ->withCount('books')
                        ->whereHas("books")
                        ->paginate();
        });

        return Inertia::render('Shelf/Public', compact('shelves'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Shelf $shelf, Request $request)
    {
        $shelf->load('user');

        $books = Cache::remember('public_shelf_'.$shelf->id.'_'.$request->get('page', 1), config('cache.bulk'), function () use ($shelf) {
            return $shelf->books()->paginate();
        });

        return Inertia::render('Shelf/Show', compact('shelf', 'books'));
    }
}
