<?php

namespace App\Http\Controllers;

use App\Device\Contracts\DeviceManager;
use App\Http\Requests\StoreDeviceRequest;
use App\Http\Requests\UpdateDeviceRequest;
use App\Models\Device;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Inertia\Inertia;
use NumberFormatter;

class DeviceController extends Controller
{
    public function __construct()
    {
        return $this->authorizeResource(Device::class, 'device');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = Auth::user()->devices()->paginate(config('app.per_page'));

        return Inertia::render('Device/Index', compact('devices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(DeviceManager $device_manager, Request $request, Session $session)
    {
        $deviceTypes = $device_manager->getDeviceTypes();

        if ($request->from) {
            $session->flash('from', $request->from);
        }

        return Inertia::render('Device/Create', compact('deviceTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDeviceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDeviceRequest $request, Session $session)
    {
        $driver = $request->type;

        $name = auth()->user()->name."'s ".$this->getNextDeviceNumberFormatted($driver).' '.Str::headline($driver);

        $device = auth()->user()
                        ->devices()
                        ->create($request->merge(
                            compact('name', 'driver')
                        )
                        ->only('name', 'driver', 'config'));

        return $session->has('from')
            ? redirect($session->get('from'))->with('success', "Your ${driver} device has been successfully added. You can rename the device or add more devices by clicking on your avatar on the top right and then devices.")
            : redirect(route('devices.edit', ['device' => $device->id]));
    }

    /**
     * Get the next device number of this driver type
     *
     * @param  string  $driver
     * @return string
     */
    private function getNextDeviceNumberFormatted($driver)
    {
        $number_formatter = new NumberFormatter('en_US', NumberFormatter::ORDINAL);

        $this_device_number = auth()->user()->devices()->driver($driver)->get()->count() + 1;

        return $number_formatter->format($this_device_number);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function edit(Device $device, DeviceManager $device_manager)
    {
        $deviceTypes = $device_manager->getDeviceTypes();

        $mode = 'edit';

        return Inertia::render('Device/Create', compact('deviceTypes', 'device', 'mode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDeviceRequest  $request
     * @param  \App\Models\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDeviceRequest $request, Device $device)
    {
        $device->update($request->only('config', 'name'));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function destroy(Device $device)
    {
        $device->delete();

        return redirect()->route('devices.index')->with('success', __(':Device successfully deleted.', ['device' => $device->name]));
    }
}
