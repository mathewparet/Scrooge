<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateBookRequest;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class BookController extends Controller
{
    public function __construct()
    {
        return $this->authorizeResource(Book::class, 'book');
    }

    public function cover($path)
    {
        return Storage::disk('library')->download($path);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $current_page = $request->get('page', 1);

        $books = Cache::tags(["public_book_list", "book_list"])->remember('public_book_list_'.$current_page, config('cache.bulk'), function () {
            return Book::with('authors')
                ->orderBy('created_at', 'desc')
                ->orderBy('published_at', 'desc')
                ->paginate(config('app.per_page'));
        });

        return Inertia::render('Book/Index', compact('books'));
    }

    public function download(Book $book, $format)
    {
        $file = $book->files()->format($format)->first();

        if (! $file) {
            return redirect()->back()->with('error', 'File not found');
        } else {
            return Storage::disk('library')->download($file->path);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        $book->load(['authors', 'files', 'tags'])
            ->setAppends([
                'can_convert',
                'missing_formats',
            ]);

        return Inertia::render('Book/Show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBookRequest  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookRequest $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}
