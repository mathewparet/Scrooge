<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Inertia\Middleware;
use Tightenco\Ziggy\Ziggy;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            'ziggy' => function () use ($request) {
                return array_merge((new Ziggy)->toArray(), [
                    'location' => $request->url(),
                ]);
            },

            'flash' => $this->exportFlashMessages($request),

            'config' => $this->getExportableConfigurations(),

            'devices' => $request->user() ? $request->user()->devices : null,

            'shelves' => $request->user() ? $request->user()->shelves()->with('books')->get() : null,

        ]);
    }

    private function exportFlashMessages(Request $request)
    {
        return [
            'info' => fn () => $request->session()->get('info'),
            'alert' => fn () => $request->session()->get('alert'),
            'error' => fn () => $request->session()->get('error'),
            'success' => fn () => $request->session()->get('success'),
        ];
    }

    private function getExportableConfigurations()
    {
        $export = [];

        foreach (config('inertia.export') as $config) {
            $export[$config] = config($config);
        }

        return $export;
    }
}
