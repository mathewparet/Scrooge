<?php

namespace App\Http\Requests;

use App\Device\Facades\DeviceManager;
use App\Rules\ValidDeviceConfig;
use Illuminate\Foundation\Http\FormRequest;

class StoreDeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'type' => 'required|in:'.implode(',', DeviceManager::getDeviceTypes(true)),
            'config' => ['required', new ValidDeviceConfig($this->type)],
        ];
    }
}
