<?php

namespace App\Http\Requests;

use App\EbookConverter\Facades\EbookConverter;
use App\Models\Device;
use App\Rules\HasConvertableSource;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @var \App\Models\Book $book
 */
class BookConversionRequest extends FormRequest
{
    /**
     * @var \App\Models\Book
     */

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return config('converter.manual')
            && (
                $this->filled('device')
                    ? $this->user()->can('sendTo', Device::find($this->post('device')))
                    : true
            );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'book' => ['required', 'exists:books,id', new HasConvertableSource],
            'format' => 'required|string|in:'.implode(',', EbookConverter::allOutputFormats()),
            'device' => 'nullable|exists:devices,id',
        ];
    }
}
