<?php

namespace App\Http\Requests;

use App\Enums\ShelfAccess;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateShelfRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * @var \App\Models\Shelf $shelf
         */
        $shelf = $this->route('shelf');

        return $this->user()->owns($shelf);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        /**
         * @var \App\Models\Shelf $shelf
         */
        $shelf = $this->route('shelf');

        return [
            'name' => [
                'required',
                'string',
                Rule::unique('shelves', 'name')
                    ->where(fn ($query) => $query->where('user_id', $this->user()->id))
                    ->ignore($shelf->id, 'id'),
            ],
            'access' => ['required', new Enum(ShelfAccess::class)],
            'type' => 'required|in:'.$shelf->type->value,
        ];
    }
}
