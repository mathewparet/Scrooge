<?php

namespace App\Http\Requests;

use App\Enums\ShelfAccess;
use App\Enums\ShelfType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class StoreShelfRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                Rule::unique('shelves', 'name')->where(fn ($query) => $query->where('user_id', $this->user()->id)),
            ],
            'access' => ['required', new Enum(ShelfAccess::class)],
            'type' => 'required|in:'.ShelfType::CUSTOM->value,
        ];
    }
}
