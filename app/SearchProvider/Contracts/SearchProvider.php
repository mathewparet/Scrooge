<?php

namespace App\SearchProvider\Contracts;

interface SearchProvider
{
    /**
     * Run advance filter for provider
     *
     * @param  string  $query - query passed by user
     * @param  array  $options - options to be passed to the provider
     * @param  object  $index - the search index provider
     * @param  string  $filters - string filters provided by the user
     * @return array
     */
    public function filter($query, $options, $index, $filters);
}
