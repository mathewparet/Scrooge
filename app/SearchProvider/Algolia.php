<?php

namespace App\SearchProvider;

use App\SearchProvider\Contracts\SearchProvider;

class Algolia implements SearchProvider
{
    public function filter($query, $options, $index, $filters)
    {
        $options['filters'] = $filters;

        return $index->search($query, $options);
    }
}
