<?php

namespace App\SearchProvider;

use App\SearchProvider\Contracts\SearchProvider;

class Meilisearch implements SearchProvider
{
    public function filter($query, $options, $index, $filters)
    {
        $options['filter'] = $filters;

        return $index->search($query, $options);
    }
}
