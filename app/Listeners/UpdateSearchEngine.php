<?php

namespace App\Listeners;

use App\Events\BookImported;

class UpdateSearchEngine
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\BookImported  $event
     * @return void
     */
    public function handle(BookImported $event)
    {
        $event->book->save();
    }
}
