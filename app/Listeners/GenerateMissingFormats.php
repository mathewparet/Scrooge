<?php

namespace App\Listeners;

use App\EbookConverter\Facades\EbookConverter;
use App\Events\BookImported;
use App\Jobs\AddNewlyConvertedBookToLibrary;
use App\Jobs\ConvertEbookFormat;
use App\Models\Book;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;

class GenerateMissingFormats
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\BookImported  $event
     * @return void
     */
    public function handle(BookImported $event)
    {
        Log::debug(__CLASS__." triggered for {$event->book->id}: {$event->book->title}");

        if (config('converter.auto', false)) {
            $this->triggerMissingFormatConversions($event->book);
        } else {
            Log::debug(__CLASS__.' Auto convertion disabled, no action will be taken');
        }
    }

    /**
     * Get file formats that are missing
     *
     * @param  \App\Models\Book  $book
     * @return array $missing_formats
     */
    private function getMissingFormats(Book $book)
    {
        $available_formats = $book->files->pluck('format')->all();

        return EbookConverter::autoMissingFormats($available_formats);
    }

    /**
     * Trigger file format conversions if there are missing formats
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    private function triggerMissingFormatConversions(Book $book)
    {
        /**
         * @var \App\Models\File $source
         */
        $source = $book->files()->whereIn('format', config('converter.input'))->first();

        if (! $source) {
            Log::debug(__CLASS__.'::'.__FUNCTION__.': No valid source file found!');

            return;
        }

        $missing_formats = $this->getMissingFormats($book);

        foreach ($missing_formats as $to_format) {
            Bus::chain([
                new ConvertEbookFormat($source, $to_format),
                new AddNewlyConvertedBookToLibrary($source, $to_format),
            ])->dispatch();

            Log::info("File convertion scheduled from {$source->path} to {$to_format}");
        }
    }
}
