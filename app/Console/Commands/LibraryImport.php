<?php

namespace App\Console\Commands;

use App\ExternalLibrary\Contracts\ExternalLibrary;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class LibraryImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'library:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Library from External Library';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(ExternalLibrary $external_library)
    {
        $external_library->import();

        $this->info(__('All books from :external has been imported/updated.', [
            'external' => class_basename($external_library),
        ]));

        Log::info('Library import completed successfully');

        return Command::SUCCESS;
    }
}
