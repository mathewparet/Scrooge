<?php

namespace App\Console\Commands;

use App\Models\Invitation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;

class CreateAdminInvitation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-admin {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an admin invitation';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $invitation = new Invitation(['email' => $this->argument('email')]);
        $invitation->user_id = 0;
        $invitation->save();

        $url = URL::temporarySignedRoute('register', now()->addDays(1), [
            'invitation_id' => $invitation->id,
            'hash' => $invitation->getHash(),
        ]);

        $this->info("Please visit the following URL to complete account creation: $url");

        return Command::SUCCESS;
    }
}
