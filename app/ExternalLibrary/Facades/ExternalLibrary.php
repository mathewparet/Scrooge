<?php

namespace App\ExternalLibrary\Facades;

use App\ExternalLibrary\Contracts\ExternalLibrary as ContractsExternalLibrary;
use Illuminate\Support\Facades\Facade;

/**
 * @method static void import() Import books
 * @method static Illuminate\Database\Eloquent\Model books() Book model for the external library
 */
class ExternalLibrary extends Facade
{
    public static function getFacadeAccessor()
    {
        return ContractsExternalLibrary::class;
    }
}
