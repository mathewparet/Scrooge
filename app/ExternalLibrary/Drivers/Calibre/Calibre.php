<?php

namespace App\ExternalLibrary\Drivers\Calibre;

use App\ExternalLibrary\Contracts\ExternalAuthor;
use App\ExternalLibrary\Contracts\ExternalBook;
use App\ExternalLibrary\Contracts\ExternalFile;
use App\ExternalLibrary\Contracts\ExternalLibrary as ContractsExternalLibrary;
use App\ExternalLibrary\Contracts\ExternalTag;
use App\ExternalLibrary\ExternalLibrary;
use Illuminate\Support\Str;

class Calibre extends ExternalLibrary implements ContractsExternalLibrary
{
    public function books()
    {
        return app(Models\Book::class);
    }

    /**
     * Get formatted tag array fromm Calibre tag object
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalTag  $tag
     * @return array
     */
    public function getTagData(ExternalTag $tag)
    {
        return [
            'name' => $tag->name,
            'external_id' => $tag->id,
        ];
    }

    /**
     * Get formatted file array fromm Calibre file object
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalFile  $file
     * @param  string  $path
     * @return array
     */
    public function getFileData(ExternalFile $file)
    {
        $format = Str::lower($file->format);

        return [
            'format' => $format,
            'size' => $file->uncompressed_size,
            'path' => $file->book()->first()->path.'/'.$file->name.'.'.$format,
            'external_id' => $file->id,
        ];
    }

    /**
     * Get formatted series array fromm Calibre series object
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalBook  $calibre_book
     * @return array
     */
    public function getSeriesData(ExternalBook $book)
    {
        $series = $book->series()->first();

        if ($series) {
            return [
                'name' => $series->name,
                'external_id' => $series->id,
            ];
        } else {
            return false;
        }
    }

    /**
     * Get formatted author array fromm Calibre author object
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalAuthor  $author
     * @return array
     */
    public function getAuthorData(ExternalAuthor $author)
    {
        return [
            'name' => $author->name,
            'external_id' => $author->id,
        ];
    }

    /**
     * Get formatted book array fromm Calibre book object
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalBook  $book
     * @return array
     */
    public function getBookData(ExternalBook $book)
    {
        return [
            'title' => $book->title,
            'published_at' => $book->published_at,
            'identifiers' => $book->identifiers()->pluck('val', 'type')->all(),
            'description' => optional($book->comment)->text ?: '',
            'path' => $book->path,
            'rating' => $book->rating()->first() ? (int) ($book->rating()->first()->rating / 2) : 0,
            'series_position' => $book->series_index,
            'image' => $book->has_cover ? $book->path.'/cover.jpg' : '',
            'external_id' => $book->id,
        ];
    }
}
