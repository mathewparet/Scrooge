<?php

namespace App\ExternalLibrary\Drivers\Calibre\Models;

use App\ExternalLibrary\Contracts\ExternalAuthor;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Author extends CalibreModel implements ExternalAuthor
{
    use HasFactory;

    public function books()
    {
        return $this->belongsToMany(Book::class, 'books_authors_link', 'author', 'book');
    }
}
