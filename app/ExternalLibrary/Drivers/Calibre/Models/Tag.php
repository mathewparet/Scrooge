<?php

namespace App\ExternalLibrary\Drivers\Calibre\Models;

use App\ExternalLibrary\Contracts\ExternalTag;

class Tag extends CalibreModel implements ExternalTag
{
    public function books()
    {
        return $this->belongsToMany(Book::class, 'books_tags_link', 'tag', 'book');
    }
}
