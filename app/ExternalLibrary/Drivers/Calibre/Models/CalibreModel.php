<?php

namespace App\ExternalLibrary\Drivers\Calibre\Models;

use Illuminate\Database\Eloquent\Model;

class CalibreModel extends Model
{
    protected $connection = 'calibre';
}
