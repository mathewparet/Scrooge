<?php

namespace App\ExternalLibrary\Drivers\Calibre\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Comment extends CalibreModel
{
    use HasFactory;

    public function book()
    {
        return $this->belongsTo(Book::class, 'book', 'id');
    }
}
