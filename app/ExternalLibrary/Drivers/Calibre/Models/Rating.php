<?php

namespace App\ExternalLibrary\Drivers\Calibre\Models;

class Rating extends CalibreModel
{
    public function books()
    {
        return $this->belongsToMany(Book::class, 'books_ratings_link', 'rating', 'book');
    }

    protected $casts = [
        'rating' => 'integer',
    ];
}
