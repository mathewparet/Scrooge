<?php

namespace App\ExternalLibrary\Drivers\Calibre\Models;

use App\ExternalLibrary\Contracts\ExternalSeries;

class Series extends CalibreModel implements ExternalSeries
{
    public function books()
    {
        return $this->belongsToMany(Book::class, 'books_series_link', 'series', 'book');
    }
}
