<?php

namespace App\ExternalLibrary\Drivers\Calibre\Models;

use App\ExternalLibrary\Contracts\ExternalFile;

class File extends CalibreModel implements ExternalFile
{
    protected $table = 'data';

    public function book()
    {
        return $this->belongsTo(Book::class, 'book', 'id');
    }
}
