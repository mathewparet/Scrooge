<?php

namespace App\ExternalLibrary\Drivers\Calibre\Models;

use App\ExternalLibrary\Contracts\ExternalBook;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Book extends CalibreModel implements ExternalBook
{
    use HasFactory;

    protected $casts = [
        'series_index' => 'integer',
    ];

    public function publishedAt(): Attribute
    {
        return Attribute::make(
            get: fn () => Carbon::parse($this->pubdate)
        );
    }

    public function identifiers()
    {
        return $this->hasMany(Identifier::class, 'book', 'id');
    }

    public function comment()
    {
        return $this->hasOne(Comment::class, 'book', 'id');
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class, 'books_authors_link', 'book', 'author');
    }

    public function series()
    {
        return $this->belongsToMany(Series::class, 'books_series_link', 'book', 'series');
    }

    public function rating()
    {
        return $this->belongsToMany(Rating::class, 'books_ratings_link', 'book', 'rating');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'book', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'books_tags_link', 'book', 'tag');
    }
}
