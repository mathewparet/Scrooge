<?php

namespace App\ExternalLibrary;

use App\Models\Tag;
use App\Models\Book;
use App\Models\Author;
use App\Models\Series;
use App\Events\BookImported;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\ExternalLibrary\Contracts\ExternalBook;
use App\ExternalLibrary\Contracts\ExternalLibrary as ContractsExternalLibrary;

/**
 * @method Illuminate\Database\Eloquent\Model books()
 */
abstract class ExternalLibrary implements ContractsExternalLibrary
{
    public function import()
    {
        $this->books()->get()->each(function ($external_book) {
            DB::transaction(function () use ($external_book) {
                $book = $this->getBookData($external_book);

                $series = $this->importSeries($external_book);

                $book = $this->addBookToSeries($series, $book);

                $this->importAuthors($external_book, $book);

                $this->importFiles($external_book, $book);

                $this->importTags($external_book, $book);

                BookImported::dispatch($book);
            });
        });

        Cache::tags(["book_list"])->flush();
    }

    /**
     * Import books from Calibre
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalBook  $calibre_book
     * @param  \App\Models\Book  $book
     * @return void
     */
    private function importFiles(ExternalBook $external_book, Book $book)
    {
        $external_book->files()->each(function ($file) use ($book) {
            $book->files()->updateOrCreate($this->getFileData($file));
        });
    }

    /**
     * Import series from Calibre
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalBook  $external_book
     * @param  \App\Models\Book  $book
     * @return void|\App\Models\Series
     */
    private function importSeries(ExternalBook $external_book)
    {
        $external_series = $this->getSeriesData($external_book);

        if ($external_series) {
            return Series::updateOrCreate(['external_id' => $external_series['external_id']], $external_series);
        } else {
            return false;
        }
    }

    /**
     * Add book to series of a series exist, else create a book without series
     *
     * @param  \App\Models\Series  $series
     * @param  array  $book
     * @return \App\Models\Book
     */
    private function addBookToSeries($series, $external_book)
    {
        $book = Book::firstOrNew(['external_id' => $external_book['external_id']], $external_book);

        $book->forceFill($external_book);

        if ($series) {
            $book = $series->books()->save($book);
        } else {
            $book->save();
        }

        return $book;
    }

    /**
     * Import authors from Calibre
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalBook  $external_book
     * @param  \App\Models\Book  $book
     * @return void
     */
    private function importAuthors(ExternalBook $external_book, Book $book)
    {
        $authors = collect();

        $external_book->authors()->each(function ($author) use (&$authors) {
            $authors->push(Author::updateOrCreate(['external_id' => $author->id], $this->getAuthorData($author)));
        });

        $book->authors()->sync($authors->pluck('id'));
    }

    /**
     * Import tags from Calibre
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalBook  $external_book
     * @param  \App\Models\Book  $book
     * @return void
     */
    private function importTags(ExternalBook $external_book, Book $book)
    {
        $tags = collect();

        $external_book->tags()->each(function ($tag) use (&$tags) {
            $tags->push(Tag::updateOrCreate(['external_id' => $tag->id], $this->getTagData($tag)));
        });

        $book->tags()->sync($tags->pluck('id'));
    }
}
