<?php

namespace App\ExternalLibrary\Contracts;

/**
 * Illuminate\Database\Eloquent\Model
 */
interface ExternalFile
{
    /**
     * Get the book for the file
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book();
}
