<?php

namespace App\ExternalLibrary\Contracts;

interface ExternalLibrary
{
    /**
     * Import books
     */
    public function import();

    /**
     * Get the book model for the external library
     *
     * @return \App\ExternalLibrary\Contracts\ExternalBook
     */
    public function books();

    /**
     * Get tag information from external database
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalTag  $tag
     * @return array|bool
     */
    public function getTagData(ExternalTag $tag);

    /**
     * Get file information from external database
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalFile  $file
     * @return array
     */
    public function getFileData(ExternalFile $file);

    /**
     * Get series information from external database
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalBook  $book
     * @return array|bool
     */
    public function getSeriesData(ExternalBook $book);

    /**
     * Get author information from external database
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalAuthor  $author
     * @return array
     */
    public function getAuthorData(ExternalAuthor $author);

    /**
     * Get book information from external database
     *
     * @param  \App\ExternalLibrary\Contracts\ExternalBook  $book
     * @return array
     */
    public function getBookData(ExternalBook $book);
}
