<?php

namespace App\ExternalLibrary\Contracts;

/**
 * Illuminate\Database\Eloquent\Model
 */
interface ExternalBook
{
    /**
     * Get all series in the book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function series();

    /**
     * Get all identifiers for the book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function identifiers();

    /**
     * Get the ratings for the book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rating();

    /**
     * Get the authors for the book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors();

    /**
     * Get the tags for the book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags();

    /**
     * Get the files for the book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files();
}
