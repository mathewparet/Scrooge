<?php

namespace App\Enums;

enum ShelfType: String
{
    case WANT_TO_READ = 'WANT_TO_READ';
    case READ = 'READ';
    case READING = 'READING';
    case CUSTOM = 'CUSTOM';
}
