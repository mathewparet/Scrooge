<?php

namespace App\Enums;

enum ShelfAccess: string
{
    case PRIVATE = 'PRIVATE';
    case PUBLIC = 'PUBLIC';
}
