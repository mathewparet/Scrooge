<?php

namespace App\Jobs;

use App\Models\Book;
use App\Models\User;
use App\Notifications\EbookReadyToDownload;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyUserWhenEbookIsConverted implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    public $book;

    public $format;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Book $book, $format)
    {
        $this->user = $user;
        $this->book = $book;
        $this->format = $format;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->notify(new EbookReadyToDownload($this->book, $this->format));
    }
}
