<?php

namespace App\Jobs;

use App\BookAPI\Contracts\BookAPI;
use App\EbookConverter\Facades\EbookConverter as FacadesEbookConverter;
use App\Models\Author;
use App\Models\Book;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @method static \Illuminate\Foundation\Bus\PendingDispatch dispatch(string $author_dir, string $book_dir)
 */
class FetchBookDetailsAndAddToLibrary implements ShouldQueue, ShouldBeUnique
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $author_dir;

    public $book_dir;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($author_dir, $book_dir)
    {
        $this->author_dir = $author_dir;
        $this->book_dir = $book_dir;
    }

    public function uniqueId()
    {
        return $this->author_dir.','.$this->book_dir;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(BookAPI $book_api)
    {
        if ($this->batch()->cancelled()) {
            return;
        }

        $response = $book_api->author($this->author_dir)->book(Str::replace($this->author_dir.'/', '', $this->book_dir))->lookup();

        $book = $this->addBookToLibrary($response);

        $authors = $this->addAuthorsToLibrary($response, $book);

        $files = $this->addFilesToLibrary($book);

        $this->convertFilesIfNeeded($book);

        Log::info('Added/updated book', ['title' => $response->title, 'authors' => $authors, 'files' => $files]);
    }

    private function convertFilesIfNeeded($book)
    {
        $source = $book->files->whereIn('format', FacadesEbookConverter::inputFormats())->first();

        if (! $source) {
            return;
        }

        $available_formats = $book->files->pluck('format')->all();

        $needed_formats = FacadesEbookConverter::autoMissingFormats($available_formats);

        foreach ($needed_formats as $output_format) {
            Bus::chain([
                new ConvertEbookFormat($source, $output_format),
                new FetchBookDetailsAndAddToLibrary($this->author_dir, $this->book_dir),
            ])->dispatch();

            Log::info("File convertion scheduled from {$source->path} to {$output_format}");
        }
    }

    private function addFilesToLibrary($book)
    {
        $files = Storage::disk('library')->files($book->path);

        $imported_files = [];

        foreach ($files as $file) {
            $path_info = pathinfo($file);

            if (in_array($path_info['extension'], config('converter.formats'))) {
                $book->files()->updateOrCreate([
                    'path' => $file,
                    'format' => $path_info['extension'],
                ]);

                $imported_files[] = $file;
            } else {
                Log::debug('Skipping invalid ebook file '.$file);
            }
        }

        return $imported_files;
    }

    private function addAuthorsToLibrary($response, $book)
    {
        $authors = [];

        $response->authors->each(function ($book_author) use ($book, &$authors) {
            $author = Author::updateOrCreate(['name' => $book_author->name]);
            $author->books()->attach($book);
            $authors[] = $book_author->name;
        });

        return $authors;
    }

    private function addBookToLibrary($response)
    {
        return Book::updateOrCreate([
            'title' => $response->title,
            'subtitle' => $response->subtitle,
            'description' => $response->description,
            'image' => $response->image,
            'page_count' => $response->page_count,
            'published_at' => $response->published_at,
            'path' => $this->book_dir,
        ]);
    }
}
