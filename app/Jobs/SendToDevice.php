<?php

namespace App\Jobs;

use App\Device\Contracts\DeviceManager;
use App\Exceptions\InvalidObjectToSendToDeviceException;
use App\Models\Book;
use App\Models\Device;
use App\Models\File;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * @method static \Illuminate\Foundation\Bus\PendingDispatch dispatch(\App\Models\File|\App\Models\Book $object, \App\Models\Device $device, string format = null)
 */
class SendToDevice implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $file;

    public $object;

    public $format;

    public $device;

    public $timeout = 0;

    public $failOnTimeout = true;

    /**
     * Create a new job instance.
     *
     * @param  \App\Models\File | \App\Models\Book  $object
     * @param  \App\Models\Device  $device
     * @param  string  $format
     * @return void
     */
    public function __construct($object, Device $device, $format = null)
    {
        $this->object = $object;

        $this->device = $device;

        $this->format = $format;
    }

    private function getFileFromObject($object, $format = null)
    {
        $class = get_class($object);

        switch($class) {
            case File::class:
                return $object;
                break;

            case Book::class:
                return $object->files()->format($format)->first();
                break;

            default:
                throw new InvalidObjectToSendToDeviceException($class);
        }
    }

    public function uniqueId()
    {
        return implode(', ', [
            get_class($this->object), $this->object->id, $this->device->id, $this->format,
        ]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DeviceManager $device_manager)
    {
        try {
            $this->file = $this->getFileFromObject($this->object, $this->format);

            Log::debug('Sending book to device', [
                'title' => $this->file->book->title,
                'device' => $this->device->name,
                'format' => $this->file->format,
            ]);

            $device_manager->device($this->device->driver)->send($this->file, $this->device->config);

            Log::info('Book successfully sent', [
                'title' => $this->file->book->title,
                'device' => $this->device->name,
                'format' => $this->format,
            ]);
        } catch(\Throwable $e) {
            Log::error('Error sending book to device', [
                'book' => $this->file->book->title,
                'format' => $this->file->format,
                'device' => $this->device,
                'exception' => $e->getMessage(),
                'stack' => $e->getTrace(),
            ]);
        }
    }
}
