<?php

namespace App\Jobs;

use App\EbookConverter\Contracts\EbookConverter;
use App\Models\File;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * @method static \Illuminate\Foundation\Bus\PendingDispatch dispatch(\App\Models\File $file, string $to_format)
 */
class ConvertEbookFormat implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $file;

    public $to;

    public $timeout = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(File $file, $to_format)
    {
        $this->file = $file;
        $this->to = $to_format;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(EbookConverter $ebook_converter)
    {
        $ebook_converter->convert($this->file, $this->to);
    }

    public function uniqueId()
    {
        return $this->file->path.','.$this->to;
    }
}
