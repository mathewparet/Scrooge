<?php

namespace App\Jobs;

use App\Models\File;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class AddNewlyConvertedBookToLibrary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $original_file;

    public $new_format;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(File $original_file, $new_format)
    {
        $this->original_file = $original_file;
        $this->new_format = $new_format;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $new_file_path = $this->getNewFilePath();

        $this->original_file->book->files()->save(new File([
            'format' => $this->new_format,
            'path' => $new_file_path,
            'size' => $this->getFileSize($new_file_path),
        ]));
    }

    private function getFileSize($new_file_path)
    {
        return Storage::disk('library')->size($new_file_path);
    }

    private function getNewFilePath()
    {
        return str_replace($this->original_file->format, $this->new_format, $this->original_file->path);
    }
}
