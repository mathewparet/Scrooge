<?php

namespace App\Providers;

use App\Device\Contracts\DeviceManager as ContractDeviceManager;
use App\Device\DeviceManager;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class DeviceServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerDeviceManager();
    }

    private function registerDeviceManager()
    {
        $this->app->singleton(ContractDeviceManager::class, function ($ap) {
            return new DeviceManager();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [
            ContractDeviceManager::class,
        ];
    }
}
