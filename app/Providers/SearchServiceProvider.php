<?php

namespace App\Providers;

use App\SearchProvider\Algolia;
use App\SearchProvider\Contracts\SearchProvider;
use App\SearchProvider\Meilisearch;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class SearchServiceProvider extends ServiceProvider implements DeferrableProvider
{
    protected $providers = [
        'algolia' => Algolia::class,
        'meilisearch' => Meilisearch::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SearchProvider::class, $this->providers[config('scout.driver')]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [
            SearchProvider::class,
        ];
    }
}
