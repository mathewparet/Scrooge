<?php

namespace App\Providers;

use App\BookAPI\Contracts\BookAPI;
use App\Exceptions\BookAPIDriverEexception;
use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;

class BookAPIServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BookAPI::class, function ($app) {
            return $this->getDriver($this->getDefaultDriver());
        });
    }

    private function getDriver($driver)
    {
        $driver_class = $this->getConfig($driver);

        throw_if(empty($driver_class), InvalidArgumentException::class, "BookAPI Driver {$driver} is not supported");

        try {
            return new $driver_class(config("book.providers.${driver}"));
        } catch(\Throwable $e) {
            throw new BookAPIDriverEexception("BookAPI Driver ${driver_class} couldn't be loaded");
        }
    }

    private function getConfig($driver)
    {
        return config("book.providers.{$driver}.driver") ?: [];
    }

    private function getDefaultDriver()
    {
        return config('book.default');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
