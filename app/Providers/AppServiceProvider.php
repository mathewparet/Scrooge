<?php

namespace App\Providers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->defineBlueprintMacros();
    }

    /**
     * Define Blueprint macros
     *
     * @return void
     */
    private function defineBlueprintMacros()
    {
        Blueprint::macro('slug', function ($name = 'slug') {
            $this->string($name);
        });
    }
}
