<?php

namespace App\Providers;

use App\ExternalLibrary\Contracts\ExternalLibrary;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class ExternalLibraryServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ExternalLibrary::class, $this->getDefaultExternalLibraryProvider());
    }

    /**
     * Identify the configured external library privider and return the class
     *
     * @return \App\ExternalLibrary\Contracts\ExternalLibrary|string
     */
    private function getDefaultExternalLibraryProvider()
    {
        $default_provider = config('external.default');

        return config("external.providers.{$default_provider}");
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [
            ExternalLibrary::class,
        ];
    }
}
