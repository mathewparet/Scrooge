<?php

namespace App\Providers;

use App\EbookConverter\Contracts\EbookConverter as Contract;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;

class EbookConvertionServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Contract::class, function ($app) {
            return $this->getDefaultDriverInstance();
        });
    }

    private function getDefaultDriverInstance()
    {
        $driver = $this->getDefaultDriver();
        $driver_engine = config("converter.providers.{$driver}.driver");

        throw_if(empty($driver_engine), InvalidArgumentException::class, "Ebook Converter Driver {$driver} not found!");

        return new $driver_engine(config("converter.providers.{$driver}"));
    }

    private function getDefaultDriver()
    {
        return config('converter.default');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [
            Contract::class,
        ];
    }
}
