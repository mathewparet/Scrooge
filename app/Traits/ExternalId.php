<?php

namespace App\Traits;

trait ExternalId
{
    public function scopeExternalId($query, $external_id)
    {
        return $query->where('external_id', $external_id);
    }
}
