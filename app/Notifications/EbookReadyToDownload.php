<?php

namespace App\Notifications;

use App\Models\Book;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EbookReadyToDownload extends Notification implements ShouldQueue
{
    use Queueable;

    public $book;

    public $format;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Book $book, $format)
    {
        $this->book = $book;
        $this->format = $format;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($user)
    {
        return (new MailMessage)
            ->subject($this->book->title.' is now ready for download')
            ->markdown('Mail.EbookReadyToDownload', [
                'book' => $this->book,
                'format' => $this->format,
                'url' => url(route('books.show', ['book' => $this->book->slug])),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
