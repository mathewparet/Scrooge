<?php

namespace App\Actions\Fortify;

use App\Models\Invitation;
use App\Models\Shelf;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        if (config('invitation.required')) {
            $session_invitation = Session::get('invitation');
            $invitation = Invitation::findOrFail($session_invitation['id']);
            abort_if($invitation->checkHash($invitation['hash']), 403, 'Unauthorized access');
        } else {
            $invitation = null;
        }

        return DB::transaction(function () use ($input, $invitation) {
            Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => $this->passwordRules(),
                'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
            ])->validate();

            $user = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
            ]);

            if (config('invitation.required')) {
                $invitation->delete();
            }

            Shelf::createForUser($user);

            return $user;
        });
    }
}
