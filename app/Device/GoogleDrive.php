<?php

namespace App\Device;

use App\Device\Contracts\Device;
use App\Models\File;
use Illuminate\Support\Facades\Validator;

class GoogleDrive extends DeviceEngine implements Device
{
    public function send(File $file, $config)
    {
        // $device->config['email'] will have the Kindle's email
    }

    public function getValidator($config)
    {
        $rules = [
            'path' => 'required|email:filter',
        ];

        return Validator::make($config, $rules);
    }
}
