<?php

namespace App\Device\Contracts;

interface DeviceManager
{
    /**
     * Get the list of supported device types
     *
     * @param  bool  $only_values = false
     * @return array
     */
    public function getDeviceTypes($only_values = false);

    /**
     * Returns the device engine for the specific device
     *
     * @param  string  $driver
     * @return \App\Device\Contracts\Device
     */
    public function device($driver);
}
