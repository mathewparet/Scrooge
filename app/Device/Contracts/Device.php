<?php

namespace App\Device\Contracts;

use App\Models\File;

interface Device
{
    /**
     * Send ebook to Device
     *
     * @param  \App\Models\File  $file
     * @param  mixed  $config
     */
    public function send(File $file, $config);

    /**
     * Get the validator for this device
     *
     * @param  mixed  $config
     * @return \Illuminate\Validation\Validator
     */
    public function getValidator($config);
}
