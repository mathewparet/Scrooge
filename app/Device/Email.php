<?php

namespace App\Device;

use App\Device\Contracts\Device;
use App\Mail\SendToEmail;
use App\Models\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class Email extends DeviceEngine implements Device
{
    public function send(File $file, $config)
    {
        Mail::to($config['email'])->send(new SendToEmail($file));
    }

    public function getValidator($config)
    {
        $rules = [
            'email' => 'required|email:filter',
        ];

        return Validator::make($config, $rules);
    }
}
