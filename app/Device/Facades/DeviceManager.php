<?php

namespace App\Device\Facades;

use App\Device\Contracts\DeviceManager as ContractsDeviceManager;
use Illuminate\Support\Facades\Facade;

/**
 * @method static array getDeviceTypes(bool $only_values = false) Get the list of supported device types
 * @method static \App\Device\Contracts\Device device(string $driver) Returns the device engine for the specific device
 */
class DeviceManager extends Facade
{
    public static function getFacadeAccessor()
    {
        return ContractsDeviceManager::class;
    }
}
