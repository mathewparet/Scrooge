<?php

namespace App\Device;

class DeviceEngine
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Get devices master config
     *
     * @param  string|null  $name
     * @return mixed
     */
    public function getConfig($name = null)
    {
        return data_get($this->config, $name);
    }

    public function missingFormats($available_formats)
    {
        return array_values(array_diff($this->getConfig('limits.formats'), $available_formats));
    }
}
