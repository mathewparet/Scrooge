<?php

namespace App\Device;

use App\Device\Contracts\DeviceManager as ContractsDeviceManager;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class DeviceManager implements ContractsDeviceManager
{
    private static $DEVICES = [];

    public function device($name)
    {
        return self::$DEVICES[$name] ??= $this->loadDevice($name);
    }

    public function getDeviceTypes($only_values = false)
    {
        return Arr::map(array_keys(config('device')), function ($value) use ($only_values) {
            return $only_values ? $value : [
                'label' => config('device.'.$value.'.label', Str::headline($value)),
                'value' => $value,
                'driver' => config('device.'.$value.'.driver'),
                'config' => config('device.'.$value.'.config'),
            ];
        });
    }

    private function loadDevice($device)
    {
        $device_driver = $this->getDeviceDriver($device);

        return self::$DEVICES[$device] = new $device_driver($this->getDeviceDriverConfig($device));
    }

    private function getDeviceDriverConfig($device)
    {
        return config("device.{$device}.config");
    }

    private function getDeviceDriver($device)
    {
        return config("device.{$device}.driver");
    }
}
