<?php

namespace App\Rules;

use App\Device\Facades\DeviceManager;
use Illuminate\Contracts\Validation\Rule;

class ValidDeviceConfig implements Rule
{
    private $driver;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return  DeviceManager::device($this->driver)->getValidator($value)->validate();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This configuration is invalid for '.ucfirst($this->driver);
    }
}
