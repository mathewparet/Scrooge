<?php

namespace App\BookAPI\Facades;

use App\BookAPI\Contracts\BookAPI as ContractsBookAPI;
use Illuminate\Support\Facades\Facade;

/**
 * @method static \App\BookAPI\Contracts\BookAPI author(string $name) Set the author to search for
 * @method static \App\BookAPI\Contracts\BookAPI book(string $title) Set the book to search for
 * @method static \App\BookAPI\DataStructures\Book lookup(string $author = null, string $book = null) Search for the book
 */
class BookAPI extends Facade
{
    public static function getFacadeAccessor()
    {
        return ContractsBookAPI::class;
    }
}
