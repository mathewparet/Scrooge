<?php

namespace App\BookAPI;

use Illuminate\Support\Facades\Http;

abstract class BooksAPIEngine
{
    protected $config;

    protected $author;

    protected $book_title;

    protected $http;

    public function __construct($config)
    {
        $this->config = $config;
        $this->http = Http::withUserAgent('mathewparet/ebook-engine');

        if (method_exists($this, 'boot')) {
            app()->call([$this, 'boot']);
        }
    }

    public function author($name)
    {
        $this->author = $name;

        return $this;
    }

    public function book($title)
    {
        $this->book_title = $title;

        return $this;
    }

    public function lookup($author = null, $book = null)
    {
        $this->author = $author ?: $this->author;
        $this->book_title = $book ?: $this->book_title;

        return app()->call([$this, 'fetch']);
    }
}
