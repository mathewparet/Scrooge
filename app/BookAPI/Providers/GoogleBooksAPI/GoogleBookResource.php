<?php

namespace App\BookAPI\Providers\GoogleBooksAPI;

use App\BookAPI\DataStructures\Book;
use Illuminate\Support\Carbon;

class GoogleBookResource extends Book
{
    public function translate($data, $authors)
    {
        return [
            'title' => $data['title'],
            'subtitle' => isset($data['subtitle']) ? $data['subtitle'] : '',
            'published_at' => Carbon::parse($data['publishedDate']),
            'description' => isset($data['description']) ? $data['description'] : '',
            'identifiers' => $data['industryIdentifiers'],
            'page_count' => isset($data['pageCount']) ? $data['pageCount'] : 0,
            'image' => isset($data['imageLinks']) ? $data['imageLinks']['thumbnail'] : '',
            'authors' => $authors,
        ];
    }
}
