<?php

namespace App\BookAPI\Providers\GoogleBooksAPI;

use App\BookAPI\BooksAPIEngine;
use App\BookAPI\Contracts\BookAPI;
use App\Exceptions\BookIdentificationException;
use Illuminate\Support\Facades\Log;

class GoogleBooksAPI extends BooksAPIEngine implements BookAPI
{
    public function fetch()
    {
        try {
            $response = $this->http->get($this->config['endpoint'].$this->buildQuery());

            return $this->parseResponse($response->json());
        } catch(\Throwable $e) {
            Log::debug(__CLASS__.'->'.__FUNCTION__, ['query' => $this->buildQuery()]);
            Log::debug(__CLASS__.'->'.__FUNCTION__, compact('response'));
            Log::error(__CLASS__.'->'.__FUNCTION__.' '.$e->getMessage());

            throw new BookIdentificationException("Unable to identify book {$this->book_title} by {$this->author}");
        }
    }

    private function parseResponse($response)
    {
        $result = $response['items'][0];
        $volumeInfo = &$result['volumeInfo'];

        $authors = GoogleAuthorResource::collect($volumeInfo['authors']);

        return new GoogleBookResource($volumeInfo, $authors);
    }

    private function buildQuery()
    {
        return '?q=inauthor:'.urlencode($this->author).' AND intitle:'.urlencode($this->book_title);
    }
}
