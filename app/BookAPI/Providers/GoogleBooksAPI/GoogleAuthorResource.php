<?php

namespace App\BookAPI\Providers\GoogleBooksAPI;

use App\BookAPI\DataStructures\Author;

class GoogleAuthorResource extends Author
{
    public function translate($author)
    {
        return [
            'name' => $author,
        ];
    }
}
