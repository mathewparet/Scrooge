<?php

namespace App\BookAPI\Contracts;

interface BootableBookAPI
{
    public function boot();
}
