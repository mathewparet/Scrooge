<?php

namespace App\BookAPI\Contracts;

interface BookAPI
{
    /**
     * Getch records from the target books api provider
     *
     * @return \App\BookAPI\DataStructures\Book
     */
    public function fetch();

    /**
     * Define the author to look for
     *
     * @param  string  $name
     * @return \App\BookAPI\BooksAPIEngine
     */
    public function author($name);

    /**
     * Define the book to search for
     *
     * @param  string  $title
     * @return \App\BookAPI\BooksAPIEngine
     */
    public function book($title);
}
