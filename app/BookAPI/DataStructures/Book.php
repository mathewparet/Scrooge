<?php

namespace App\BookAPI\DataStructures;

abstract class Book
{
    protected $data;

    public function __construct($data, $authors)
    {
        $this->data = $this->translate($data, $authors);
    }

    abstract public function translate($data, $authors);

    public function __get($name)
    {
        return $this->data[$name];
    }
}
