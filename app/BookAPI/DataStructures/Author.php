<?php

namespace App\BookAPI\DataStructures;

abstract class Author
{
    private $data;

    public function __construct($data)
    {
        $this->data = $this->translate($data);
    }

    abstract public function translate($author);

    public function __get($name)
    {
        return $this->data[$name];
    }

    public static function collect($authors)
    {
        $data = collect([]);

        foreach ($authors as $author) {
            $class = get_called_class();
            $data->push(new $class($author));
        }

        return $data;
    }
}
