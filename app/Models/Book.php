<?php

namespace App\Models;

use App\EbookConverter\Facades\EbookConverter;
use App\Traits\ExternalId;
use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * @method static \Illuminate\Database\Eloquent\Builder ExternalId(int $calibre_id) - Filter by Calibre's ID
 */
class Book extends Model
{
    use HasFactory;
    use Searchable;
    use ExternalId;
    use Sluggable;

    protected function getSluggableAttribute()
    {
        return 'title';
    }

    protected $perPage = 16;

    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'image',
        'published_at',
        'page_count',
        'path',
        'identifiers',
        'series_position',
        'rating',
        'external_id',
    ];

    protected $casts = [
        'published_at' => 'date:d M Y',
        'identifiers' => 'json',
    ];

    protected $hidden = [
        'path',
    ];

    protected $appends = [
        'published_year',
    ];

    protected $with = [
        'series',
    ];

    protected function getPublishedYearAttribute()
    {
        return $this->published_at->format('Y');
    }

    public function toSearchableArray()
    {
        $base_fields = [
            'id' => (int) $this->id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'published_at' => $this->published_at,
            'description' => $this->description,
            'identifiers' => $this->identifiers,
            'image' => route('books.cover', ['path' => $this->path.'/cover.jpg']),
            'page_count' => (int) $this->page_count,
            'published' => (int) $this->published_year,
            'series_position' => (int) $this->series_position,
            'rating' => (int) $this->rating,
            'tag' => $this->tags ? $this->tags->pluck('name') : [],
            'author' => $this->authors->pluck('name'),
        ];

        if ($this->series) {
            $base_fields = array_merge($base_fields, [
                'series' => optional($this->series)->name,
            ]);
        }

        return $base_fields;
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function scopeTitle($query, $title)
    {
        return $query->whereTitle($title);
    }

    public function scopePath($query, $path)
    {
        return $query->wherePath($path);
    }

    public function series()
    {
        return $this->belongsTo(Series::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function getConvertableFile()
    {
        return $this->files()->whereIn('format', config('converter.input'))->first();
    }

    public function getCanConvertAttribute()
    {
        return $this->files()
                ->whereIn('format', config('converter.input'))
                ->get()
                ->count() > 0
            && config('converter.manual');
    }

    public function getMissingFormatsAttribute()
    {
        return EbookConverter::allMissingFormats($this->files->pluck('format')->all());
    }

    public function shelves()
    {
        return $this->belongsToMany(Shelf::class);
    }

    protected static function booted()
    {
        static::deleting(function ($book) {
            $book->tags()->detach();
            $book->files->each->delete();
            $book->authors()->detach();
        });
    }
}
