<?php

namespace App\Models;

use App\Device\Facades\DeviceManager;
use Illuminate\Database\Eloquent\Casts\AsEncryptedArrayObject;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        'config', 'name', 'driver',
    ];

    protected $casts = [
        'config' => AsEncryptedArrayObject::class,
    ];

    protected $appends = ['supported_formats'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeDriver($query, $driver)
    {
        return $query->where('driver', $driver);
    }

    /**
     * Get engine for current driver
     *
     * @return \App\Device\DeviceEngine
     */
    public function engine()
    {
        return DeviceManager::device($this->driver);
    }

    public function supportedFormats(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->engine()->getConfig('limits.formats')
        );
    }
}
