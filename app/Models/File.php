<?php

namespace App\Models;

use App\Traits\ExternalId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static \Illuminate\Database\Eloquent\Builder ExternalId(int $calibre_id) - Filter by Calibre's ID
 */
class File extends Model
{
    use HasFactory;
    use ExternalId;

    protected $fillable = [
        'format', 'path', 'size', 'external_id',
    ];

    public function scopeFormat($query, $format)
    {
        return $query->whereFormat($format);
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
