<?php

namespace App\Models;

use App\Traits\Sluggable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use Sluggable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    protected function syncSlugWithAttribute()
    {
        return true;
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function suspend($suspend = true)
    {
        $this->suspended = $suspend;

        return $this->save();
    }

    public function activate($activate = true)
    {
        $this->suspended = ! $activate;

        return $this->save();
    }

    public function scopeSuspended($query, $suspended = true)
    {
        return $query->whereSuspended($suspended);
    }

    public function scopeAvtive($query, $active = true)
    {
        return $query->whereSuspended(! $active);
    }

    /**
     * Check if model is owned by user
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return bool
     */
    public function owns(Model $model)
    {
        return $model->user_id == $this->id;
    }

    /**
     * Check if logged in user is super user
     *
     * @return bool
     */
    public function isSuperUser()
    {
        return $this->id == 1;
    }

    public function addDevice(Device $device)
    {
        return $this->devices()->save($device);
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }

    public function shelves()
    {
        return $this->hasMany(Shelf::class);
    }
}
