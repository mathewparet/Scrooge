<?php

namespace App\Models;

use App\Traits\ExternalId;
use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static \Illuminate\Database\Eloquent\Builder ExternalId(int $calibre_id) - Filter by Calibre's ID
 */
class Tag extends Model
{
    use HasFactory;
    use ExternalId;
    use Sluggable;

    public function makeSlugUnique()
    {
        return false;
    }

    protected $perPage = 20;

    protected $fillable = [
        'name', 'external_id',
    ];

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
