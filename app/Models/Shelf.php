<?php

namespace App\Models;

use App\Enums\ShelfAccess;
use App\Enums\ShelfType;
use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Shelf extends Model
{
    use HasFactory;
    use HasUuids;
    use Sluggable;

    public function makeSlugUnique()
    {
        return false;
    }

    protected $fillable = [
        'name', 'type', 'access',
    ];

    protected $casts = [
        'type' => ShelfType::class,
        'access' => ShelfAccess::class,
    ];

    protected function syncSlugWithAttribute()
    {
        return true;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }

    /**
     * Create initial set of shelves for user
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public static function createForUser(User $user)
    {
        foreach (ShelfType::cases() as $shelf_type) {
            if ($shelf_type == ShelfType::CUSTOM) {
                continue;
            }

            $user->shelves()->create([
                'name' => Str::of($shelf_type->value)->title()->replace('_', ' ')->value,
                'access' => ShelfAccess::PRIVATE->value,
                'type' => $shelf_type->value,
            ]);
        }
    }

    public function scopeWantToRead($query)
    {
        return $query->where('type', ShelfType::WANT_TO_READ);
    }

    protected static function booted()
    {
        static::deleting(function ($shelf) {
            $shelf->books()->detach();
        });
    }

    public function scopePublic($query)
    {
        return $query->where('access', ShelfAccess::PUBLIC);
    }

    public function getRouteKeyName()
    {
        return 'id';
    }
}
