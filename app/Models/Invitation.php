<?php

namespace App\Models;

use App\Mail\InvitationToRegister;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Invitation extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
    ];

    protected $appends = [
        'expires_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function booted()
    {
        static::saved(function ($invitation) {
            Mail::send(new InvitationToRegister($invitation));
        });
    }

    public function expiresAt(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->created_at->addDays(config('invitation.expiry'))
        );
    }

    public function getHash()
    {
        return sha1($this->email);
    }

    public function checkHash($hash)
    {
        return $hash == $this->getHash();
    }
}
