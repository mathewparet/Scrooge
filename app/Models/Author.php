<?php

namespace App\Models;

use App\Traits\ExternalId;
use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static \Illuminate\Database\Eloquent\Builder ExternalId(int $calibre_id) - Filter by Calibre's ID
 */
class Author extends Model
{
    use HasFactory;
    use ExternalId;
    use Sluggable;

    protected $perPage = 16;

    protected $fillable = [
        'name', 'external_id',
    ];

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }

    public function scopeName($query, $name)
    {
        return $query->whereName($name);
    }
}
