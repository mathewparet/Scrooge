@extends('errors::minimal')

@section('title', __('Not Found'))
@section('code')
<iframe src="https://embed.lottiefiles.com/animation/8616"></iframe>
@endsection
@section('message', "Oh no! We are looking for that too! Let us know if you find it!")
