@extends('errors::minimal')

@section('title', __('Under Maintenance'))
@section('code')
<iframe src="https://embed.lottiefiles.com/animation/99349"></iframe>
@endsection
@section('message')
Time for a break :)  {{config("app.name")}} is currently under maintenance. Please check back later
@endsection
