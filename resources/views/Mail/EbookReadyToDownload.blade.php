<x-mail::message>
# {{$book->title}} is now ready to download in {{$format}} format.

We have converted {{$book->title}} to {{$format}} format as requested and is now ready to download. Please click the button below to download the book.

<x-mail::button url="{{$url}}">
Download Now
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
