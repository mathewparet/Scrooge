<x-mail::message>
# {{$book->title}} by {{$author->name}}

Please find the book attached.

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
