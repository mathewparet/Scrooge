<x-mail::message>
# Welcome to {{ config('app.name') }},

{{ $user->name }} has invited you to register for an account at {{ config('app.name') }}.

If you do not wish to accept this invitation, you do not need to do anything. This invitation will expire in {{ $expiry }} days.

<x-mail::button :url="$url">
Accept & Register Account
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
