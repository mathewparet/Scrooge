export default {
    data()
    {
        return [
            { label: 'Books', route: route('books.index'), active: !route().current().indexOf('books.') },
            { label: 'Authors', route: route('authors.index'), active: !route().current().indexOf('authors.') },
            { label: 'Tags', route: route('tags.index'), active: !route().current().indexOf('tags.') },
            { label: 'Series', route: route('series.index'), active: !route().current().indexOf('series.') },
            { label: 'Shelves', route: route('public.shelves.index'), active: !route().current().indexOf('public.shelves.') },
        ];
    }
}